Integrantes:																	

	Tito Marcos de Moraes Klautau - 11/0142390 - titoklautau@gmail.com 			
	Elton Araujo de Castro - 11/0028384 - eltonaraujodecastro@gmail.com 		
    Victor Fernandes Uriarte - 11/0021193 - uriarte0505@gmail.com  				
    Roberto Nishino Ono - 10/0122272 - ronishino@gmail.com 						
    Alexandro G. da R. Gonçalves - 10/0023967 -alexandror2@yahoo.com.br 		

Informações:
	
	Repositório: https://bitbucket.org/TMKlautau/trabalho-so

	Hangout do trabalho: https://hangouts.google.com/call/xgkkzbizxbaxbneduqeduifpvqu
	
	Topologia a ser utilizada: B(Torus)

Instruções para compilação

	usar comando make na pasta /src
	os 3 arquivos gerados serão:
		executa_postergado
		escalonador
		shutdown

	no arquivo toolbox.h a variavel do pré-processador DEBUGMODE aciona o modo de debug ao ser setada em 1 (necessário recompilar após modifica-la)

Especificação da implementação

	toolbox:

		depende de:
			nada

		Neste trabalho toolbox.h contém somente a diretiva de pré-processamento para debug

	msgs:

		depende de:
			toolbox

		A classe msgs é um encapsulamento das primitivas de filas de msgs, criei para portabilidade (prefiro windows, infelizmente o bash de windowns não tem a primitiva de fila de msgs implementada
		ainda, assim encapsulei ela para depois se tivesse tempo portar o programa para windos, infelizmente como tive que terminar as pressar não consegui encapsular as outras primitivas, como fork)
		Como avisado pela professora a implementação deveria conter no maximo 12 listas de mensagens, assim nesta implementação somente foi empregada 3:
			msgGerentsC : responsável pela passagem de mensagem entre os nós do torus, e entre o nó (1,1) e o processo auxiliar de cada job.

			msgQexec : responsavel pela passagem de novos jobs, a comunicação entre o auxiliar de jobs e o gestor de gerentes, e por final utilizado pelo programa shutdown para enviar o sinal ao gestor
			(para isso ia usar signal, mas infelizmente um signal não pode ser setado para um método não static de uma classe,e passar o processo de shutdown que já tinha implementado para um método
			static gastaria mais tempo, futuramente irei revisar para ser implementado por sinal);

			shutdownList : responsavel por armazenar o pid de todos os processos criados, para a chacina descriminada quando o processo shutdown é chamado

	executa_postergado:

		depende de:
			msgs / toolbox

		O programa executa_postergado segue a especificação dada, infelizmente somente aceita dois argumentos, o primeiro é o delay e o segundo o path para o programa a ser executado
		(como somente aceita dois argumentos o programa não pode receber nenhum argumento próprio, em futura implementação arrumarei isso), o programa cria um link com a fila de mensagens msgQexec
		e envia o seu Pid pelo channel 1 (o tipo de msg neste trabalho sera usado como channel), após isso espera um handshake do escalonador (ExecRequestServer) pelo channel de número do pid enviado,
		após isso este envia o delay e o path para o ExecRequestServer e recebe deste o numero do Job atribuido a ele, após isto imprime as informações do processo colocado e sua execução é terminada.
		

	gerente:

		depende de:
			msgs / toolbox

		Define a classe de gerente, os objetos dessa classe são criados com uma posição na matriz 4x4 que define o tórus, o processo gerente se divide em duas partes importantes:

			timeKeeper: responsável por executar o processo a ser postergado, um por vez, e retornar o startTime e o finishTime.

			gerent: responsável por passar informações ao timeKeeper, e servir de ponto de passagem de mensagens no torus, por definição cada node somente pode se comunicar com seus adjacentes,
			e somente o (1,1) pode se comunicar com o gestor de gerentes do escalonador (o algoritimo de calculo de rota no torus implementado é abismal, sim eu tenho vergonha dele, infelizmente
			por motivos de saúde e tempo é o que foi feito, funciona pelo menos)

	escalonador:

		depende de:
			msgs / toolbox / gerente

		o coração do programa, o escalonador é um objeto único (não defini como singleton por burrice, deveria o ter feito, é boa prática) mas se subdivide nos seguintes processos:
			ExecRequestServer: responsável por receber as informações das instâncias de executa_postergado

			Gestor: processo que armazena as informações sobre os jobs efetuados e que ainda faltam executar, para cada novo job que este recebe do ExecRequestServer é criado
			um novo processo JobAux, é de responsabilidade do gestor receber a mensagem do processo shutdown, imprimir o relatório (este tbm imprime a linha de termino de
			processos) e efetuar o processo de shutdown.

			JobAux: Criado um para cada job, este efetua o delay necessário e gera (informação sobre o seu job) e recebe (começo e termino das execuções) as mensagens para o
			node (1,1), ao final da execução este calcula o makeSpan, envia as informações para o Gestor, e termina sua execução (unico processo importante que não é um
			loop), mais de um JobAux pode estar ativo a cada momento, mas não mais que um job é executado nos processos gerentes.

	shutdown:

		depende de:
			msgs

		Ao ser executado somente envia a mensagem de shutdown para o Gestor.

	helloworld:

		depende de:
			nada

		programa simples de helloworld usado para testes, também foi usado variantes com sleeps para o teste do calculo de makespan


Filas de msgs:

	msgQexec:
		key: 5678
		chanels:
			1: enviado pelo executa_postergado e recebido pelo execRequestServer, o conteudo da mensagem é o pid do programa executa_postergado para gerar novo canal de comunicação.

			pid: enviado pelo execRequestServer e recebido pelo executa_postergado, junto com o chanel 2 forma o canal de comunicação entre o executa postergado e o escalonador

			2: enviado pelo executa_postergado após o handshake com o execRequestServer, recebido pelo execRequestServer, junto com o channel pid forma o canal de comunicação entre o executa postergado e o escalonador

			3: recebido pelo Gestor, enviado pelo execRequestServer, é utilizado caso o Gestor receba uma mensagem de modo newjob no channel 4, recebe os dados no novo job

			4: recebido pelo Gestor, enviado pelo execRequestServer, as mensagens enviadas possuem 3 modos, newjob redireciona para o channel 3, jobFinished redireciona para o channel 5, e shutdown inicia o processo de shutdown

			5: recebido pelo Gestor, enviado pelo jobAux, é utilizado caso o Gestor receba uma mensagem de modo jobFinished, o conteudo da mensagem consiste do numero do job que foi terminado

	msgGerentsC
		key: 8760
		channels:
			nodePosition: recebido pelo gerente no node naquela posição, é utilizado para enviar mensagens diversas ao node naquela posição no torus

			550+jobNumber: recebido pelo jobAux, utilizado para passar os tempos de start e finish do job

	shutdownList
		key: 9898
			Serve como storage dos pids dos processos criados para ser utilizado no processo de shutdown

	jobWaitList
		key: 9797
			Serve como storage dos jobs que esperam serem executados

Responsáveis pela implementação de cada arquivo:

	escalonador.cpp: Tito Klautau //base implementada dia 2017-05-16
	escalonador.h: Tito Klautau
	executa_postergado.cpp: Tito Klautau //primeira versão funcional implementada dia 2017-05-16
	gerente.cpp: Tito Klautau
	gerente.h: Tito Klautau
	helloworld.cpp: Tito Klautau
	mainEscalonador.cpp: Tito Klautau
	makefile: Tito Klautau
	msgs.cpp: Tito Klautau
	msgs.h: Tito Klautau
	shutdown.cpp: Tito Klautau
	toolbox.h: Tito Klautau
	
Bugs Conhecidos:
	em resolução:
		escalonador entra em deadlock se ocorrer muitas chamadas de executa_postergado em um periodo de tempo muito curto (menos de 1 segundo)
		as filas de msgs estão chegando no max, para resolver é só criar outras listas de msgs ou alterar como é passado as informações, problema criado por colocar em usar o minimo de lista de msgs e má implementação :/ mas ainda dará para concertar utilizando muito menos que 12 lsita de msgs :D

		o que precisa para resolver: 
			criar mais listas para os gerentes, ou colocar um limite para receber novos jobs
			
	para resolver:
		programa passado ao executa_postergado não aceita parametros próprios (somente pode ser passado programas sem parametros)
