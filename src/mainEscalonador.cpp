//============================================================================
// Name        : mainEscalonador.cpp
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================

#include "escalonador.h"

int main(){

	Escalonador escalonador;

	escalonador.startEscalonador();

	return 0;
}