/*
 * escalonador.h
 *
 *  Created on: may 23, 2017
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_ESCALONADOR_H_
#define SOURCE_HEADERS_ESCALONADOR_H_

#include <iostream>
#include <unistd.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <string>
#include "toolbox.h"
#include "msgs.h"
#include <vector>
#include <sstream> 
#include "gerente.h"
#include <climits>
#include <signal.h>

class Escalonador{

public:

	Escalonador();

	void startEscalonador();

private:

	void startExecRequestServer();

	void startGestor();

	void startJobAux(int job, int delay, std::string path);

	void initGerents();

	struct waitingExecution{
		int job;
		int delay;
		int jobAuxPid;
		std::string path;
		int finished;
		int makespan;

	};

	int xMaxTorus, yMaxTorus;

	key_t keyExec, keyGerent, keyShutdown, keyWaitList;

	MsgModule msgQexec;

	MsgModule msgGerentsC;

	MsgModule shutdownList;

	MsgModule jobWaitList;

	int pid, jobCount, waitingExecutionCount, pidExecRequestServer;

	int executionGerentsPids [4][4];

	std::vector<Gerente> executionGerentsVector;

	std::vector<waitingExecution> waitingList;

};


#endif /* SOURCE_HEADERS_ESCALONADOR_H_ */