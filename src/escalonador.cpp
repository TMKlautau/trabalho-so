//============================================================================
// Name        : escalonador.cpp
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================

#include "escalonador.h"

Escalonador::Escalonador() :keyExec(5678), msgQexec(keyExec, 0666), keyGerent(8760), msgGerentsC(keyGerent, 0666) ,keyShutdown(9898), shutdownList(keyShutdown, 0666), keyWaitList(9797), jobWaitList(keyWaitList, 0666), xMaxTorus(4), yMaxTorus(4), jobCount(1), waitingExecutionCount(0){
	this->pid = ::getpid();


	#if DEBUGMODEPIDS == 1
		std::cout << "PID Gestor: " << this->pid << std::endl << std::endl;
	#endif

}

void Escalonador::startEscalonador(){

	msgQexec.createMsgQueue();
	shutdownList.createMsgQueue();
	msgGerentsC.createMsgQueue();
	jobWaitList.createMsgQueue();

	this->initGerents();

	for(int i = 0; i < 4; i++){
		for(int j = 0; j < 4; j++){
			shutdownList.sendMsg(1,(char*) static_cast<std::ostringstream*>( &(std::ostringstream() << this->executionGerentsPids[i][j]) )->str().c_str());
		}
	}

	if((pidExecRequestServer = ::fork()) < 0){
		std::cout << "Error: falha no fork" << std::endl;
	} 
	if (pidExecRequestServer == 0){

		this->startExecRequestServer();	

	}else{

		#if DEBUGMODEPIDS == 1
				std::cout << "PID ExecRequestServer: " << pidExecRequestServer << std::endl << std::endl;
		#endif

		shutdownList.sendMsg(1,(char*) static_cast<std::ostringstream*>( &(std::ostringstream() << pidExecRequestServer) )->str().c_str());

		this->startGestor();

	}	
}

void Escalonador::initGerents(){

	for(int i = 0; i < this->xMaxTorus; i++){
		for(int j = 0; j < this->yMaxTorus; j++){
			if((this->executionGerentsPids[i][j] = ::fork()) < 0){
				std::cout << "Error: falha no fork" << std::endl;
			}
			if (this->executionGerentsPids[i][j] == 0){

				this->executionGerentsVector.push_back(Gerente(i+1, j+1));

				this->executionGerentsVector.back().startGerent();
			}else{
				#if DEBUGMODEPIDS == 1
					std::cout << "PID Gerente " << (i+1)*10 + (j+1) << " : " << this->executionGerentsPids[i][j] << std::endl << std::endl;
				#endif
			}
		}
	}
}


void Escalonador::startGestor(){

	int delay, job;

	std::string path;

	msgQexec.linkMsgQueue();

	while(1){
		msgQexec.receiveMsg(4);

		std::string mode;

		mode = msgQexec.msgList.back().mtext;

		if(mode == "newjob"){ //newjob

			if(msgQexec.receiveMsg(3) < 0){ 
				#if DEBUGMODE == 1
					std::cout << "Gestor: Falha msg Gestor:" << std::endl << std::endl;
				#endif

				break;
			}

			job = strtol(msgQexec.msgList.back().mtext,NULL,10);

			msgQexec.receiveMsg(3);

			delay = strtol(msgQexec.msgList.back().mtext,NULL,10);

			#if DEBUGMODE == 1
				std::cout << "Gestor:  Delay recebido:" << delay << std::endl << std::endl;
			#endif

			msgQexec.receiveMsg(3);

			path = msgQexec.msgList.back().mtext;

			#if DEBUGMODE == 1
				std::cout << "Gestor:  Path recebido:" << path << std::endl << std::endl;
			#endif

			this->waitingList.push_back(waitingExecution());

			this->waitingList.back().job = jobCount;

			this->waitingList.back().delay = delay;

			this->waitingList.back().path = path;

			this->waitingList.back().finished = 0;

			this->waitingExecutionCount++;

			if((this->waitingList.back().jobAuxPid = ::fork()) < 0){
				std::cout << "Gestor: Error: falha no fork" << std::endl;
			} 
			if (this->waitingList.back().jobAuxPid == 0){

				this->startJobAux(jobCount, delay, path);

			}else{

				#if DEBUGMODEPIDS == 1
					std::cout << "PID JobAux " << jobCount << " : " << this->waitingList.back().jobAuxPid << std::endl << std::endl;
				#endif

			}

			shutdownList.sendMsg(100+jobCount,(char*) static_cast<std::ostringstream*>( &(std::ostringstream() << this->waitingList.back().jobAuxPid) )->str().c_str());

			jobCount++;

		}else if(mode == "jobfinished"){ // retornou processo

			msgQexec.receiveMsg(5);

			std::string lastMsg;

			lastMsg = msgQexec.msgList.back().mtext;

			int job = (int) lastMsg[0];

			lastMsg.erase(0, 1);

			int makeSpan = strtol(lastMsg.c_str(),NULL,10);

			waitingList[job-1].makespan = makeSpan;

			waitingList[job-1].finished = 1;

			std::cout << "job=" << job << ", arquivo=" << waitingList[job-1].path << ", delay=" << waitingList[job-1].delay << " segundos, makespan: " << waitingList[job-1].makespan << " segundos" << std::endl;

			::waitpid(-1, NULL, 0);

		}else if(mode == "shutdown"){
			std::cout << "Processos não terminados: " << std::endl;

			for(int i = 0; i < this->waitingList.size(); i++){
				if(waitingList[i].finished == 0){
					std::cout << "	job=" << this->waitingList[i].job << ", arquivo=" << this->waitingList[i].path << ", delay=" << this->waitingList[i].delay << " segundos" << std::endl;
				}
			}

			std::cout << "Processos terminados: " << std::endl;

			for(int i = 0; i < this->waitingList.size(); i++){
				if(waitingList[i].finished == 1){
					std::cout << "	job=" << this->waitingList[i].job << ", arquivo=" << this->waitingList[i].path << ", delay=" << this->waitingList[i].delay << " segundos, makespan: " << this->waitingList[i].makespan << " segundos" << std::endl;
				}
			}

			while(shutdownList.receiveMsgNowait(0) > 0){
				int auxPid = strtol(shutdownList.msgList.back().mtext,NULL,10);

				::kill(auxPid, SIGKILL);
			}

			msgQexec.removeMsgQueue();

			msgGerentsC.removeMsgQueue();

			shutdownList.removeMsgQueue();

			jobWaitList.removeMsgQueue();

			exit(0);
		}

	}

}

void Escalonador::startJobAux(int job, int delay, std::string path){

	::sleep(delay);

	msgGerentsC.linkMsgQueue();

	path.insert(0,1, (char) 11);

	path.insert(1,1, (char) 1);

	path.insert(2,1, (char) job);


	for(int i = 1; i < 5; i++){
		for(int j = 1; j < 5; j++){

			path[0] = (char) ((i*10)+j);
			msgGerentsC.sendMsg(11, (char*) path.c_str());

		}
	}

	std::string lastMsg;

	long int firstStartTime = LONG_MAX, lastFinishTime = 0;

	int waitingTimes = 32;

	while (waitingTimes > 0){
 
		msgGerentsC.receiveMsg(550+job);

		lastMsg = msgGerentsC.msgList.back().mtext;

		if(lastMsg[0] == (char) 1){
			lastMsg.erase(0,1);

			int aux = strtol(lastMsg.c_str(),NULL,10);

			if (firstStartTime > aux){
				firstStartTime = aux;
			}

		}else if(lastMsg[0] == (char) 2){

			lastMsg.erase(0,1);

			int aux = strtol(lastMsg.c_str(),NULL,10);


			if (lastFinishTime < aux){
				lastFinishTime = aux;
			}
		}

		waitingTimes--;

	}
	
	int makeSpan = lastFinishTime - firstStartTime;

	msgQexec.sendMsg(4, (char*) "jobfinished");

	std::string sentMsg;

	sentMsg = (char*) static_cast<std::ostringstream*>( &(std::ostringstream() << makeSpan) )->str().c_str();

	sentMsg.insert(0, 1, (char) job);

	msgQexec.sendMsg(5, (char*) sentMsg.c_str());

	shutdownList.receiveMsg(100+job);

	exit(0);

}

void Escalonador::startExecRequestServer(){

	int rPid;

	while(1){

		if(msgQexec.receiveMsg(1) < 0){ 
			#if DEBUGMODE == 1
				std::cout << "ExecRequestServer:  Execution terminated:" << std::endl << std::endl;
			#endif

			break;

		}

		rPid = strtol(msgQexec.msgList.back().mtext,NULL,10); 

		#if DEBUGMODE == 1
			std::cout << "ExecRequestServer:  Pid recebido:" << rPid << std::endl << std::endl;
		#endif

		#if DEBUGMODE == 1
			std::cout << "ExecRequestServer:  Enviado mensagem ao gerente informando sobre novo job:" << std::endl << std::endl;
		#endif
		msgQexec.sendMsg(4, (char*)"newjob");

		#if DEBUGMODE == 1
			std::cout << "ExecRequestServer:  Enviado mensagem ao gerente informando jobcount:" << std::endl << std::endl;
		#endif
		msgQexec.sendMsg(3, (char*) static_cast<std::ostringstream*>( &(std::ostringstream() << jobCount) )->str().c_str());

		msgQexec.sendMsg(rPid, (char*) "done");

		#if DEBUGMODE == 1
			std::cout << "ExecRequestServer:  Testando handshake:" << std::endl << std::endl;
		#endif

		msgQexec.receiveMsg(2);

		msgQexec.sendMsg(3, msgQexec.msgList.back().mtext);

		msgQexec.sendMsg(rPid, (char*) static_cast<std::ostringstream*>( &(std::ostringstream() << jobCount) )->str().c_str());

		msgQexec.receiveMsg(2);	

		msgQexec.sendMsg(3, msgQexec.msgList.back().mtext);

		jobCount++;

		#if DEBUGMODE == 1
			std::cout << "ExecRequestServer:  Esperando nova instancia de executa_postergado" << std::endl << std::endl;
		#endif

	}

}

