//============================================================================
// Name        : shutdown.cpp
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================

#include "msgs.h"

int main(){

	key_t keyExec = 5678;

	MsgModule msgQexec(keyExec, 0666);

	msgQexec.linkMsgQueue();

	msgQexec.sendMsg(4, (char*) "shutdown");

	return 0;
}