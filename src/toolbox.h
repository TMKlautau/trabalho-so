//============================================================================
// Name        : toolbox.h
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================


#ifndef SOURCE_HEADERS_TOOLBOX_H_
#define SOURCE_HEADERS_TOOLBOX_H_

/*
 * DEBUGMODE 1 para colocar em debugmode, 0 para execução sem feedback
 */

#define DEBUGMODESWITCH 1

#define DEBUGMODE 0
#define DEBUGMODEMSGS 0
#define DEBUGMODEPIDS 1







#if (!DEBUGMODESWITCH)

	#undef DEBUGMODE 
	#undef DEBUGMODEMSGS
	#undef DEBUGMODEPIDS

#endif

#endif /* SOURCE_HEADERS_TOOLBOX_H_ */