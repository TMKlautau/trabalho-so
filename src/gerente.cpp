//============================================================================
// Name        : gerente.cpp
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================

#include "gerente.h"


Gerente::Gerente(int posXIN, int posYIN): posX(posXIN), posY(posYIN) ,keyShutdown(9898), shutdownList(keyShutdown, 0666), keyWaitList(9797), jobWaitList(keyWaitList, 0666){

	this->pid = ::getpid();

	this->myPos = (posXIN*10)+posYIN;

	shutdownList.linkMsgQueue();
	jobWaitList.linkMsgQueue();

};

void Gerente::timeKeeper(){

	key_t key = 8760;

	MsgModule msgGerentsC(key, 0666);

	msgGerentsC.linkMsgQueue();

	jobWaitList.linkMsgQueue();

	std::string timeMsg;
	
	int jobAtual;

	while(1){
		jobWaitList.receiveMsg(this->myPos);
		
		this->msgBuffer = jobWaitList.msgList.back().mtext;

		jobAtual = (int) msgBuffer[2];
		
		msgBuffer.erase(0,3);

		#if DEBUGMODE == 1
			std::cout << "timeKeeper node: "<< this->myPos << " recebido para execução processo: " << msgBuffer << " no node: " << this->myPos << std::endl << std::endl;
		#endif

		int pidFilho;

		if((pidFilho = ::fork()) < 0){
			std::cout << "Error: falha no fork" << std::endl;
		} 
		if (pidFilho == 0){
			::execl(msgBuffer.c_str(), msgBuffer.c_str(), NULL);		
			std::cout << "Error: falha no execl" << std::endl;
		}else{

		shutdownList.sendMsg(this->myPos,(char*) static_cast<std::ostringstream*>( &(std::ostringstream() << pidFilho) )->str().c_str());
		
		long int startTime = static_cast<long int>(std::time(0));
		
		timeMsg = (char*) static_cast<std::ostringstream*>( &(std::ostringstream() << startTime) )->str().c_str();
	
		timeMsg.insert(0,1, (char) this->myPos);

		timeMsg.insert(1,1, (char) 2);

		timeMsg.insert(2,1, (char) jobAtual);
		
		msgGerentsC.sendMsg(this->myPos,(char*) timeMsg.c_str());
	
		::waitpid(-1, NULL, 0);

		shutdownList.receiveMsg(this->myPos);
		
		long int finishTime = static_cast<long int>(std::time(0));	
		
		timeMsg = (char*) static_cast<std::ostringstream*>( &(std::ostringstream() << finishTime) )->str().c_str();
	
		timeMsg.insert(0,1, (char) this->myPos);

		timeMsg.insert(1,1, (char) 3);

		timeMsg.insert(2,1, (char) jobAtual);
		
		msgGerentsC.sendMsg(this->myPos,(char*) timeMsg.c_str());
	
		}
	}

}

void Gerente::startGerent(){

	key_t key = 8760;

	MsgModule msgGerentsC(key, 0666);

	msgGerentsC.linkMsgQueue();

	int pidFilho;
	
	if((pidFilho = ::fork()) < 0){
		std::cout << "Error: falha no fork" << std::endl;
	} 
	if (pidFilho == 0){

		#if DEBUGMODE == 1
			std::cout << "Gerente node: "<< this->myPos << " iniciado timeKeeper no node: " << this->myPos << std::endl << std::endl;
		#endif
		
		this->timeKeeper();
	
	}else{

		#if DEBUGMODEPIDS == 1
			std::cout << "PID timeKeeper " << this->myPos << " : " << pidFilho << std::endl;
		#endif

	}

	shutdownList.sendMsg(1,(char*) static_cast<std::ostringstream*>( &(std::ostringstream() << pidFilho) )->str().c_str());


	while(1){

		msgGerentsC.receiveMsg(this->myPos);

		this->msgBuffer = msgGerentsC.msgList.back().mtext;

		if(msgBuffer[0] == this->myPos){

			if(msgBuffer[1] == 1){

			jobWaitList.sendMsg(this->myPos, (char*) this-> msgBuffer.c_str());

			}else if(msgBuffer[1] == 2){

				#if DEBUGMODE == 1
					std::cout << "Gerente node: "<< this->myPos << " começada a execução do processo de job: " << msgBuffer[2] << " no node: " << this->myPos << std::endl << std::endl;
				#endif
				
				msgBuffer[0] = (char) 11;
				
				msgBuffer[1] = (char) 4;
				
				if(this->myPos%10 == 1){
					msgGerentsC.sendMsg(this->myPos+1,(char*) this-> msgBuffer.c_str()); //picaretagem eu sei, mas to sem tempo e ainda nao to muito bem de saude
				}else{
					msgGerentsC.sendMsg(this->myPos-1,(char*) this-> msgBuffer.c_str());
				}


			}else if(msgBuffer[1] == 3){

				#if DEBUGMODE == 1
					std::cout << "Gerente node: "<< this->myPos << " terminada a execução do processo de job: " << msgBuffer[2] << " no node: " << this->myPos << std::endl << std::endl;
				#endif
				
				msgBuffer[0] = (char) 11;
				
				msgBuffer[1] = (char) 5;
				
				if(this->myPos%10 == 1){
					msgGerentsC.sendMsg(this->myPos+1,(char*) this-> msgBuffer.c_str());
				}else{
					msgGerentsC.sendMsg(this->myPos-1,(char*) this-> msgBuffer.c_str());
				}

			}else if(msgBuffer[1] == 4){			
			
				int jobStarted = (int) msgBuffer[2];
				
				msgBuffer.erase(0,2);
				
				msgBuffer[0] = 1;
				
				msgGerentsC.sendMsg(550+jobStarted,(char*) this-> msgBuffer.c_str());
					
			}else if(msgBuffer[1] == 5){
			
				int jobFinished = (int) msgBuffer[2];
				
				msgBuffer.erase(0,2);
				
				msgBuffer[0] = 2;
				
				msgGerentsC.sendMsg(550+jobFinished,(char*) this-> msgBuffer.c_str());
	
			}else{
			
				#if DEBUGMODE == 1
					std::cout << "Gerente node: "<< this->myPos << " Tipo de mensagem não especificada, tipo: " << (int) msgBuffer[1] << " no node: " << this->myPos << std::endl << std::endl;
				#endif
			}


		}else if((msgBuffer[0]/10) == this->myPos/10){ //se não for o recipiente redireciona a msg
			if(msgBuffer[0]%10 < this->myPos%10){
				msgGerentsC.sendMsg(this->myPos-1,(char*) this-> msgBuffer.c_str());
			}else{
				msgGerentsC.sendMsg(this->myPos+1, (char*) this-> msgBuffer.c_str());
			}
		}else{
			if(msgBuffer[0]/10 < this->myPos/10){
				msgGerentsC.sendMsg(this->myPos-10,(char*) this-> msgBuffer.c_str());
			}else{
				msgGerentsC.sendMsg(this->myPos+10, (char*) this-> msgBuffer.c_str());
			}
		}
	}
}

// msg recebida, primeiro char é o recipiente, segundo é o tipo, o resto é a informação