/*
 * gerente.h
 *
 *  Created on: may 23, 2017
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_GERENTE_H_
#define SOURCE_HEADERS_GERENTE_H_

#include <iostream>
#include "msgs.h"
#include <string>
#include <unistd.h>
#include <ctime>
#include <sstream> 
#include <sys/wait.h>


class Gerente{

public:

	Gerente(int posXIN, int posYIN);

	void startGerent();

private:

	key_t keyShutdown, keyWaitList;

	MsgModule shutdownList;

	MsgModule jobWaitList;

	int pid, posX, posY, myPos, timeKeeperPid;

	std::string msgBuffer;
	
	void timeKeeper();

};



#endif /* SOURCE_HEADERS_GERENTE_H_ */