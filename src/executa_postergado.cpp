//============================================================================
// Name        : executa_postergado.cpp
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================

#include <iostream>
#include <unistd.h>
#include <sys/shm.h>
#include <stdlib.h>
#include "toolbox.h"
#include "msgs.h"
#include <string>
#include <sstream> 

int main(int argc, char* argv[]) {
	
	if(argc != 3){
		std::cout << "Error: forneca 2 argumentos, o primeiro o numero de segundos a ser postergado, o segundo o programa a ser executado" << std::endl;
		return 0;
	}
	
	int pid, job;
	int delay = strtol(argv[1],NULL,10);
  	key_t key;
  	std::string aux;

	pid = ::getpid();

	key = 5678;
	
	MsgModule msgQexec(key, 0666);

	msgQexec.linkMsgQueue();

	aux = static_cast<std::ostringstream*>( &(std::ostringstream() << pid) )->str();

	msgQexec.sendMsg(1, (char*) aux.c_str());

	#if DEBUGMODE == 1
		std::cout << "Pid enviado: " << pid << std::endl;
	#endif

	if (msgQexec.receiveMsg(pid) < 0){
		#if DEBUGMODE == 1
			std::cout << "Erro no handshake: " << std::endl;
		#endif
		return 0;
	}

	#if DEBUGMODE == 1
		std::cout << "Handshake feito " << pid << std::endl;
	#endif

	aux = static_cast<std::ostringstream*>( &(std::ostringstream() << delay) )->str();

	msgQexec.sendMsg(2, (char*) aux.c_str());

	#if DEBUGMODE == 1
		std::cout << "Delay enviado: " << delay << std::endl;
	#endif

	if (msgQexec.receiveMsg(pid) < 0){
		#if DEBUGMODE == 1
			std::cout << "Erro no handshake 2: " << std::endl;
		#endif
		return 0;
	}

	msgQexec.sendMsg(2, argv[2]);

	std::cout << "job=" << msgQexec.msgList.back().mtext << ", arquivo=" << argv[2] << ", delay=" << delay << " segundos" << std::endl;


	#if DEBUGMODE == 1
		std::cout << "Terminada a execucao" << std::endl;
	#endif

	return 0;
}
