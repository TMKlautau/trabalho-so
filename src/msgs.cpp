//============================================================================
// Name        : msgs.cpp
// Author      : Tito Klautau
// Version     : 0.1
// Copyright   : 
// Description : Trabalho de SO
//============================================================================

#include "msgs.h"

MsgModule::MsgModule(key_t keyIn, int msgPermIn): key(keyIn), msgPerm(msgPermIn){}

void MsgModule::createMsgQueue(){
	this->msgId = msgget(this->key, this->msgPerm|IPC_CREAT|IPC_EXCL);

	if((msgId = msgget(this->key, this->msgPerm|IPC_CREAT)) < 0){
		std::cout << "Error createMsgQueue: " << std::endl;
		perror("msgget");
	}else{
		#if DEBUGMODEMSGS == 1
			std::cout << "Created Msg Queue: key - " << this->key << " msgPerm: " << this->msgPerm << std::endl << std::endl;
		#endif
	}

}

void MsgModule::linkMsgQueue(){
	this->msgId = msgget(this->key, this->msgPerm);

	#if DEBUGMODEMSGS == 1
		if(msgId < 0){
			std::cout << "Error linkMsgQueue: " << std::endl;
			perror("msgget");
		}else{
			std::cout << "Linkado Msg Queue: key - " << this->key << " msgPerm: " << this->msgPerm << std::endl << std::endl;
		}
	#endif
}

void MsgModule::sendMsg(long mtypeIn, char* mtextIn){
	struct msg_struct auxStruct;
	auxStruct.mtype = mtypeIn;
	strcpy(auxStruct.mtext, mtextIn);

	int aux = msgsnd(this->msgId, &auxStruct, MSGTXTLEN, 0);

	#if DEBUGMODEMSGS == 1
		if(aux < 0){
			std::cout << "Error sendMsg: " << std::endl;
			perror("msgsnd");
		}else{
			std::cout << "Mensage sent: Type - " << auxStruct.mtype << " text: " << auxStruct.mtext << std::endl << std::endl;
		}
	#endif
}

int MsgModule::receiveMsg(long mtypeIn){
	this->msgList.push_back(msg_struct());

	int aux = msgrcv(this->msgId, &this->msgList.back(), MSGTXTLEN, mtypeIn, 0);

	#if DEBUGMODEMSGS == 1
		if(aux < 0){
			std::cout << "Error receiveMsg: " << std::endl;
			perror("msgrcv");
		}else{
			std::cout << "Mensage received: Type - " << this->msgList.back().mtype << " text: " << this->msgList.back().mtext << std::endl << std::endl;
		}
	#endif

	return aux;
}

int MsgModule::receiveMsgNowait(long mtypeIn){
	this->msgList.push_back(msg_struct());

	int aux = msgrcv(this->msgId, &this->msgList.back(), MSGTXTLEN, mtypeIn, IPC_NOWAIT);

	#if DEBUGMODEMSGS == 1
		if(aux < 0){
			std::cout << "Error receiveMsg: " << std::endl;
			perror("msgrcv");
		}else{
			std::cout << "Mensage received: Type - " << this->msgList.back().mtype << " text: " << this->msgList.back().mtext << std::endl << std::endl;
		}
	#endif

	return aux;
}

void MsgModule::removeMsgQueue(){
	int auxRmv = msgctl(this->msgId, IPC_RMID, 0);

	#if DEBUGMODEMSGS == 1
		if(auxRmv < 0){
			std::cout << "Error removeMsgQueue: " << std::endl;

		}else{
			std::cout << "Removida Msg Queue: key - " << this->key << " msgPerm: " << this->msgPerm << std::endl << std::endl;
		}
	#endif
}