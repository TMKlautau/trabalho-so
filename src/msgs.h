/*
 * msgs.h
 *
 *  Created on: may 23, 2017
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_MSGS_H_
#define SOURCE_HEADERS_MSGS_H_

#include <sys/ipc.h>
#include <sys/msg.h>
#include <vector>
#include <iostream>
#include "toolbox.h"
#include <string.h>
#include <cstdio>

#define MSGTXTLEN 64   

struct msg_struct{
  long mtype;
  char mtext[MSGTXTLEN];
};

class MsgModule{
public:
	MsgModule(key_t keyIn, int msgPermIn);

	key_t key;

	int msgPerm;

	int msgId;

	void createMsgQueue();

	void linkMsgQueue();
	
	void removeMsgQueue();

	void sendMsg(long mtypeIn, char* mtextIn);

	int receiveMsg(long mtypeIn);

	int receiveMsgNowait(long mtypeIn);


	std::vector<msg_struct> msgList;

private:

};


#endif /* SOURCE_HEADERS_MSGS_H_ */